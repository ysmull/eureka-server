#!/usr/bin/env bash
kill `ps -aux | grep keep-ip.py | awk 'NR==1{print $2}'`
python ./keep-ip.py