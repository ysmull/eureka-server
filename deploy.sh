#!/usr/bin/env bash
pwd
echo "-trying to stop container..."
sudo docker stop eureka-server
echo "-trying to rm container..."
sudo docker rm eureka-server
echo "-trying to rmi image..."
sudo docker rmi eureka-server
echo "-building image..."
sudo docker build -t eureka-server .
echo "-running image..."
echo "-expose port 8888"
sudo docker run -d --name="eureka-server" -p 8888:8888 eureka-server
echo "-assign ip address 172.17.0.110 172.17.0.110/24@172.17.0.1"
sudo pipework docker0 eureka-server 172.17.0.110/24@172.17.0.1
#
#kill `ps -aux | grep keep-ip.py | awk 'NR==1{print $2}'`
#python ./keep-ip.py

