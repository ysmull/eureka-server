FROM oracle/serverjre:8
COPY target/eureka-1.0-SNAPSHOT.jar /
CMD [ "java", "-jar",  "eureka-1.0-SNAPSHOT.jar"]